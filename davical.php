<?php
  $c->domain_name = getenv('HOST_NAME')?:'davical.example.net';
  $c->admin_email = getenv('ADMIN_EMAIL')?:'admin@example.net';
  $c->sysabbr = 'davical';
  $c->system_name = "DAViCal CalDAV Server";  
  $c->pg_connect[] = "dbname=davical user=davical_app";
  
  $c->restrict_setup_to_admin = true;
  $c->default_privileges = array('all');
  $c->template_usr = array('active' => true,
                           'locale' => getenv('LOCALE')?:'en_EN',
                           'date_format_type' => 'E',
                           'email_ok' => date('Y-m-d'));
  $c->default_locale = getenv('LOCALE')?:'en_EN';
  $c->metrics_style = 'counters';
  $c->metrics_require_user = 'admin';
?>
