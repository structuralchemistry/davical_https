#!/bin/bash

backupfile=$1

createdb=/usr/lib/postgresql/11/bin/createdb
pgrestore=/usr/lib/postgresql/11/bin/pg_restore
psql=/usr/lib/postgresql/11/bin/psql
pgoptions='-U postgres -Fc --clean'
database=davical

if [ $# -eq 0 ] ; then
  echo
  echo "  restore_db.sh:"
  echo
  echo "  A pg_dump backup file needs to be supplied."
  echo
  exit
fi

su - postgres -c "${psql} -c \"drop database ${database};\""
su - postgres -c "${createdb} --owner davical_dba --encoding UTF8 --template template0 ${database}"

if [[ $backupfile =~ \.gz$ ]] ; then
  zcat ${backupfile} | su - postgres -c "${pgrestore} ${pgoptions} -d ${database}"
else
  su - postgres -c "${pgrestore} ${pgoptions} -d ${database} ${backupfile}"
fi

/usr/share/davical/dba/update-davical-database


