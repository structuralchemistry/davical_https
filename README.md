# DAViCal Docker Container
Docker image for a DAViCal server based on Debian 10 (davical, awl, php, apache, postgresql, perl).

<a href=https://www.davical.org/>DAViCal</a> is a server for shared calendars implementing the <a href=https://wikipedia.org/wiki/CalDAV>CalDAV protocol</a>. The calendars are stored in the <a href=https://wikipedia.org/wiki/ICalendar>iCalendar format</a>.

## Settings
* Exposed Ports: TCP 80 and TCP 443
* Exposed Volumes: /var/lib/postgresql/data/, /etc/apache2/ssl/ and /var/log/httpd
* Exposed Volumes: /var/lib/postgresql/data/ and /etc/apache2/ssl/
* Exposed Variables: ADMIN_EMAIL, DB_NAME, DB_USER, DB_PWD, LOCALE, SERVER_NAME and TIME_ZONE

### Exposed volumes
__/var/lib/postgresql/data__: contains the database and can be mounted to a host directory to obtain a persistent database.

__/etc/apache2/ssl__: this volume needs to be mounted to a host directory that contains the files cert.pem (SSL certificate) and cert-key.pem (SSL certificate key).

__/var/log/httpd__: contains the apache log files and can be mounted to a host directory to obtain persistent log files.

### Automated backup
A daily dump of the database is saved in the directory __/var/lib/postgresql/data/backup__. The database can be restored from the backup using the script __/usr/sbin/restore_db.sh__.

## Docker example
```
docker run -d --name davical -p 8443:443 -v /var/davical/data:/var/lib/postgresql/data:Z -v /var/davical/ssl:/etc/apache2/ssl:Z -v /var/davical/httpd:/var/log/httpd:Z -e SERVER_NAME='calendar.myhost.com' -e TIME_ZONE='Europe/Berlin' structuralchemistry/davical_https
```
Creates and runs a DAViCal Docker container which will be accessible on the host system on TCP port 8443 (HTTPS). The time zone is set to Europe/Berlin and the server name is calendar.myhost.com. The directory /var/davical/data will contain the database files and the backup directory. The directory /var/davical/ssl on the host must exist and contain the certificate/key files (named cert.pem and cert-key.pem, respectively) for calendar.myhost.com. The directory /var/davical/httpd will contain the apache log files.

## Credits
Inspired by <a href=https://github.com/datze/davical>datze/davical</a>.
