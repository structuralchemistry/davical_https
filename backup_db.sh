#!/bin/sh

backupdir=/var/lib/postgresql/data/backup

pgdump=/usr/lib/postgresql/11/bin/pg_dump
pgoptions='-U postgres -Fc'
database=davical

gzip=/bin/gzip

year=`date +%Y` 
month=`date +%m`
day=`date +%d`
hour=`date +%H`
min=`date +%M`
today=$year$month$day-$hour$min

su postgres -c "${pgdump} ${pgoptions} ${database} > ${backupdir}/${database}_${today}.pgdump"
rm -f ${backupdir}/${database}_*.pgdump.gz
$gzip ${backupdir}/${database}_${today}.pgdump
chmod go-rwx ${backupdir}/${database}_${today}.pgdump.gz

