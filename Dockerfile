# Dockerised DAViCal calendar server
#   davical, awl, php, apache, postgresql, perl
#
# version 0.9, ah, 2021-05-22
#
# DAViCal Installation instructions:
#   https://www.davical.org/installation.php
#

FROM php:8.0-apache

ENV TIME_ZONE   "Europe/Berlin"
ENV LOCALE      "en_EN"
ENV SERVER_NAME "davical.example.net"
ENV ADMIN_EMAIL "admin@example.net"
ENV DB_NAME     "davical"
ENV DB_USER     "admin"
ENV DB_PWD      "dfH#s82E"

RUN apt-get update

# set timezone
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata
RUN ln -fs /usr/share/zoneinfo/$TIME_ZONE /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata
RUN apt-get remove -y tzdata

# install php-calendar
RUN docker-php-ext-install calendar

# install php-curl
RUN apt-get install -y libcurl4-openssl-dev
RUN docker-php-ext-install curl

# install php-gettext
RUN docker-php-ext-install gettext

# install php-iconv
RUN docker-php-ext-install iconv

# install php-imap
# https://stackoverflow.com/questions/13436356/configure-error-utf8-mime2text-has-new-signature-but-u8t-canonical-is-missi
RUN apt-get install -y libc-client-dev libkrb5-dev
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl
RUN docker-php-ext-install imap

# install php-ldap
RUN apt-get install -y libldb-dev libldap2-dev
RUN docker-php-ext-install ldap

# install postgresql, php-pdo, php-pdo_pqsql
RUN apt-get install -y postgresql postgresql-client
RUN apt-get install -y libpq-dev
RUN docker-php-ext-install pdo
RUN docker-php-ext-install pdo_pgsql

# install php-xml
RUN apt-get install -y libxml2-dev
RUN docker-php-ext-install xml

# install perl
RUN apt-get install -y libyaml-perl
RUN apt-get install -y libdbd-pg-perl
RUN apt-get install -y libdbi-perl

# install awl, davical
RUN apt-get install -y git
RUN git clone https://gitlab.com/davical-project/awl.git /usr/share/awl/
RUN git clone https://gitlab.com/davical-project/davical.git /usr/share/davical/
RUN rm -rf /usr/share/davical/.git /usr/share/awl/.git/
RUN apt-get remove -y git
COPY davical.php /etc/davical/config.php

# apache config
COPY httpd-vhost.conf /etc/apache2/sites-available/000-default.conf
COPY ipblacklist_wizcrafts.conf /etc/apache2/ipblacklist.conf
RUN a2enmod ssl
RUN a2enmod rewrite

# periodic backup
COPY backup_db.sh /etc/cron.daily/backup
RUN chmod 0755 /etc/cron.daily/backup

# restore_db script
COPY restore_db.sh /usr/sbin/restore_db.sh
RUN chmod 0755 /usr/sbin/restore_db.sh

# entrypoint
COPY docker-entrypoint.sh /sbin/docker-entrypoint.sh
RUN chmod 0755 /sbin/docker-entrypoint.sh

# cleanup
RUN rm -rf /tmp/*
RUN rm -rf /var/cache/apk/*
RUN rm -rf /var/lib/apt/lists/*
RUN rm -rf /var/tmp/*

EXPOSE 80 443

VOLUME 	["/var/lib/postgresql/data/", "/etc/apache2/ssl/", "/var/log/httpd"]

ENTRYPOINT ["/sbin/docker-entrypoint.sh"]
