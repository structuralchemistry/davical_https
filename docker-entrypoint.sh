#!/bin/bash

# initialise database at first run
if [ ! -e /var/lib/postgresql/data/pg_hba.conf ]; then
  chown -R postgres:postgres /var/lib/postgresql
  su - postgres -c "/usr/lib/postgresql/11/bin/initdb -D data"
  echo "listen_addresses='*'" >> /var/lib/postgresql/data/postgresql.conf
  echo "log_destination = 'syslog'" >> /var/lib/postgresql/data/postgresql.conf
  echo "syslog_facility = 'LOCAL1'" >> /var/lib/postgresql/data/postgresql.conf
  echo "timezone = $TIME_ZONE" >> /var/lib/postgresql/data/postgresql.conf
  sed -i "/# Put your actual configuration here/a local   davical    davical_app   trust\nlocal   davical    davical_dba   trust" /var/lib/postgresql/data/pg_hba.conf
  mkdir /var/lib/postgresql/data/backup
  chown -R postgres:postgres /var/lib/postgresql/data/backup
fi

# start postgresql server
su - postgres -c "/usr/lib/postgresql/11/bin/pg_ctl -D /var/lib/postgresql/data/ start"

# create database if it doesn't exist
INITIALIZED_DB=$(su postgres -c "psql -l" | grep -c davical)
if [[ $INITIALIZED_DB == 0 ]]; then
  chmod u+x /usr/share/davical/dba/create-database.sh
  su postgres -c "/usr/share/davical/dba/create-database.sh $DB_NAME $DB_PWD"
fi
unset INITIALIZED_DB

# php config
cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
sed -i "/;include_path = \".:/php/includes\"/include_path = \".:/usr/share/awl/inc:/usr/share/davical/inc\"" /usr/local/etc/php/php.ini

# apache config
echo "export SERVER_NAME=$SERVER_NAME" >> /etc/apache2/envvars
ln -s /usr/share/davical/htdocs /var/www/html/davical

# update the database
/usr/share/davical/dba/update-davical-database

# start cron
/etc/init.d/cron start

# start apache
apachectl -D FOREGROUND

